# coding=utf8
# the above tag defines encoding for this document and is for Python 2.x compatibility

import re

regexComp = r"^\\newcommand{\\companyName}\{\S+\}"
regexPos  = r"^\\newcommand{\\positionName}\{[\w\s]*}"

with open('main_resume.tex', 'r') as myfile:
    data=myfile.read()

with open('main_resume.tex', 'r') as myfile:
    data2=myfile.read()
matchesPos   = re.finditer(regexPos, data, re.MULTILINE)
matchesComp  = re.finditer(regexComp, data2, re.MULTILINE)
print(matchesPos)
print(matchesComp)

for matchNum, match in enumerate(matchesPos):
    matchNum = matchNum + 1
    print('matches found')
    print ("Match {matchNum} was found at {start}-{end}: {match}".format(matchNum = matchNum, start = match.start(), end = match.end(), match = match.group()))
    mathcesFirstPos = match.group()
    for groupNum in range(0, len(match.groups())):
        groupNum = groupNum + 1
        
        print ("Group {groupNum} found at {start}-{end}: {group}".format(groupNum = groupNum, start = match.start(groupNum), end = match.end(groupNum), group = match.group(groupNum)))
        
for matchNum, match in enumerate(matchesComp):
    matchNum = matchNum + 1
    
    print ("Match {matchNum} was found at {start}-{end}: {match}".format(matchNum = matchNum, start = match.start(), end = match.end(), match = match.group()))
    mathcesFirstComp = match.group()
    for groupNum in range(0, len(match.groups())):
        groupNum = groupNum + 1
        
        print ("Group {groupNum} found at {start}-{end}: {group}".format(groupNum = groupNum, start = match.start(groupNum), end = match.end(groupNum), group = match.group(groupNum)))

print(mathcesFirstComp)
print(mathcesFirstPos)
compName = mathcesFirstComp[25:].strip("{}") 
jobName = mathcesFirstPos[26:].strip("{}")
import os

import datetime
  # Returns 2018-01-15
def create_new_file(file_name):
    """
    Check if a directory exists, if not create and cd it.
    """
    if not os.path.exists(file_name):
        with open("FallCJD2018.csv", "w") as myfile:
            myfile.write("Companies,Jobs,Date\n")
create_new_file('FallCJD2018.csv')
 
outputString = '%s , %s, %s' % (compName, jobName, datetime.date.today())
with open("FallCJD2018.csv", "a") as myfile:
     myfile.write(outputString)
     
     
import os
import subprocess


def execute_shell_command(cmd, work_dir):
    """Executes a shell command in a subprocess, waiting until it has completed.

    :param cmd: Command to execute.
    :param work_dir: Working directory path.
    """
    pipe = subprocess.Popen(cmd, shell=True, cwd=work_dir, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    (out, error) = pipe.communicate()
    print(out, error)
    pipe.wait()


def git_add(file_path, repo_dir):
    """Adds the file at supplied path to the Git index.
    File will not be copied to the repository directory.
    No control is performed to ensure that the file is located in the repository directory.

    :param file_path: Path to file to add to Git index.
    :param repo_dir: Repository directory.
    """
    cmd = 'git add ' + file_path
    execute_shell_command(cmd, repo_dir)


def git_commit(commit_message, repo_dir):
    """Commits the Git repository located in supplied repository directory with the supplied commit message.

    :param commit_message: Commit message.
    :param repo_dir: Directory containing Git repository to commit.
    """
    cmd = 'git commit -m "%s"' % commit_message
    execute_shell_command(cmd, repo_dir)


def git_push(repo_dir):
    """Pushes any changes in the Git repository located in supplied repository directory to remote git repository.

    :param repo_dir: Directory containing git repository to push.
    """
    cmd = 'git push '
    execute_shell_command(cmd, repo_dir)


def git_clone(repo_url, repo_dir):
    """Clones the remote Git repository at supplied URL into the local directory at supplied path.
    The local directory to which the repository is to be clone is assumed to be empty.

    :param repo_url: URL of remote git repository.
    :param repo_dir: Directory which to clone the remote repository into.
    """
    cmd = 'git clone ' + repo_url + ' ' + repo_dir
    execute_shell_command(cmd, repo_dir)

commitString = '%s --- %s as of %s' % (compName, jobName, datetime.date.today())

dir_path = os.path.dirname(os.path.realpath(__file__))
git_add (".",dir_path)

git_commit ( commitString,dir_path)

git_push(dir_path)
#print(mathSlice)
# Note: for Python 2.7 compatibility, use ur"" to prefix the regex and u"" to prefix the test string and substitution.